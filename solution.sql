-- Charmaine Mae I. Ramirez


-- a. Find all artists that has letter D in its name.
SELECT * FROM artists WHERE name LIKE "%d%";
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady Gaga     |
|  6 | Ariana Grande |
+----+---------------+

-- b. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;
+----+--------------+----------+---------------------------------------+----------+
| id | song_name    | length   | genre                                 | album_id |
+----+--------------+----------+---------------------------------------+----------+
|  3 | Pardon Me    | 00:02:23 | Rock                                  |        1 |
|  4 | Stellar      | 00:02:00 | Rock                                  |        1 |
|  6 | Love Story   | 00:02:13 | Country                               |        3 |
|  8 | Red          | 00:02:04 | Country                               |        4 |
|  9 | Black Eyes   | 00:01:01 | Rock and Roll                         |        5 |
| 10 | Shallow      | 00:02:01 | Country, Rock, Folk Rock              |        5 |
| 12 | Sorry        | 00:01:32 | Dancehall-poptropical housemoombahton |        7 |
| 13 | Boyfriend    | 00:01:32 | Pop                                   |        8 |
| 15 | Thank U Next | 00:01:56 | Pop, R&B                              |       10 |
| 16 | 24K Magic    | 00:02:07 | Funk, Disco, R&B                      |       11 |
| 17 | Lost         | 00:01:52 | Pop                                   |       12 |
+----+--------------+----------+---------------------------------------+----------+

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
 SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id;
 +-----------------+----------------+----------+
| album_title     | song_name      | length   |
+-----------------+----------------+----------+
| Make yourself   | Pardon Me      | 00:02:23 |
| Make yourself   | Stellar        | 00:02:00 |
| Psy 6           | Gangnam Style  | 00:02:53 |
| Fearless        | Fearless       | 00:02:46 |
| Fearless        | Love Story     | 00:02:13 |
| Red             | State of Grace | 00:02:43 |
| Red             | Red            | 00:02:04 |
| A Star is Born  | Black Eyes     | 00:01:01 |
| A Star is Born  | Shallow        | 00:02:01 |
| Born This Way   | Born This Way  | 00:02:52 |
| Purpose         | Sorry          | 00:01:32 |
| Believe         | Boyfriend      | 00:01:32 |
| Dangerous Woman | Into You       | 00:02:42 |
| Thank U, Next   | Thank U Next   | 00:01:56 |
| 24K Magic       | 24K Magic      | 00:02:07 |
| Earth to Mars   | Lost           | 00:01:52 |
+-----------------+----------------+----------+

-- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";
+----+---------------+----+-----------------+---------------+-----------+
| id | name          | id | album_title     | date_released | artist_id |
+----+---------------+----+-----------------+---------------+-----------+
|  1 | Incubus       |  1 | Make yourself   | 1999-10-26    |         1 |
|  3 | Taylor Swift  |  3 | Fearless        | 2008-11-11    |         3 |
|  4 | Lady Gaga     |  5 | A Star is Born  | 2018-10-10    |         4 |
|  4 | Lady Gaga     |  6 | Born This Way   | 2011-06-29    |         4 |
|  6 | Ariana Grande |  9 | Dangerous Woman | 2016-05-20    |         6 |
|  6 | Ariana Grande | 10 | Thank U, Next   | 2019-02-08    |         6 |
|  7 | Bruno Mars    | 11 | 24K Magic       | 2016-11-18    |         7 |
|  7 | Bruno Mars    | 12 | Earth to Mars   | 2011-01-28    |         7 |
+----+---------------+----+-----------------+---------------+-----------+

-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
+----+---------------+---------------+-----------+
| id | album_title   | date_released | artist_id |
+----+---------------+---------------+-----------+
| 10 | Thank U, Next | 2019-02-08    |         6 |
|  4 | Red           | 2012-10-22    |         3 |
|  7 | Purpose       | 2015-11-13    |         5 |
|  2 | Psy 6         | 2012-01-15    |         2 |
+----+---------------+---------------+-----------+

-- f. Join the 'albums' and 'songs' table. (Sort albums from Z-A and sort songs from A-Z.)
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;
+----+-----------------+---------------+-----------+----+----------------+----------+---------------------------------------+----------+
| id | album_title     | date_released | artist_id | id | song_name      | length   | genre                                 | album_id |
+----+-----------------+---------------+-----------+----+----------------+----------+---------------------------------------+----------+
| 10 | Thank U, Next   | 2019-02-08    |         6 | 15 | Thank U Next   | 00:01:56 | Pop, R&B                              |       10 |
|  4 | Red             | 2012-10-22    |         3 |  8 | Red            | 00:02:04 | Country                               |        4 |
|  4 | Red             | 2012-10-22    |         3 |  7 | State of Grace | 00:02:43 | Rock, Alternative Rock, Arena Rock    |        4 |
|  7 | Purpose         | 2015-11-13    |         5 | 12 | Sorry          | 00:01:32 | Dancehall-poptropical housemoombahton |        7 |
|  2 | Psy 6           | 2012-01-15    |         2 |  1 | Gangnam Style  | 00:02:53 | K-POP                                 |        2 |
|  1 | Make yourself   | 1999-10-26    |         1 |  3 | Pardon Me      | 00:02:23 | Rock                                  |        1 |
|  1 | Make yourself   | 1999-10-26    |         1 |  4 | Stellar        | 00:02:00 | Rock                                  |        1 |
|  3 | Fearless        | 2008-11-11    |         3 |  5 | Fearless       | 00:02:46 | Pop Rock                              |        3 |
|  3 | Fearless        | 2008-11-11    |         3 |  6 | Love Story     | 00:02:13 | Country                               |        3 |
| 12 | Earth to Mars   | 2011-01-28    |         7 | 17 | Lost           | 00:01:52 | Pop                                   |       12 |
|  9 | Dangerous Woman | 2016-05-20    |         6 | 14 | Into You       | 00:02:42 | EDM House                             |        9 |
|  6 | Born This Way   | 2011-06-29    |         4 | 11 | Born This Way  | 00:02:52 | Electopop                             |        6 |
|  8 | Believe         | 2012-06-15    |         5 | 13 | Boyfriend      | 00:01:32 | Pop                                   |        8 |
|  5 | A Star is Born  | 2018-10-10    |         4 |  9 | Black Eyes     | 00:01:01 | Rock and Roll                         |        5 |
|  5 | A Star is Born  | 2018-10-10    |         4 | 10 | Shallow        | 00:02:01 | Country, Rock, Folk Rock              |        5 |
| 11 | 24K Magic       | 2016-11-18    |         7 | 16 | 24K Magic      | 00:02:07 | Funk, Disco, R&B                      |       11 |
+----+-----------------+---------------+-----------+----+----------------+----------+---------------------------------------+----------+